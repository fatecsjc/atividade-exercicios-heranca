package br.edu.fatecsjc;

public class Administrador extends Empregado {

    private double ajudaDeCusto;

    public Administrador() {
    }

    Administrador(String nome, String endereco, String telefone) {
        super(nome, endereco, telefone);
    }

    public Administrador(String nome, String endereco, String telefone, int codigoSetor, double salarioBase, double impostos) {
        super(nome, endereco, telefone, codigoSetor, salarioBase, impostos);
    }

    public Administrador(String nome, String endereco, String telefone, int codigoSetor, double salarioBase, double impostos, double ajudaDeCusto) {
        super(nome, endereco, telefone, codigoSetor, salarioBase, impostos);
        this.ajudaDeCusto = ajudaDeCusto;
    }

    private double getAjudaDeCusto() {
        return ajudaDeCusto;
    }

    void setAjudaDeCusto(double ajudaDeCusto) {
        this.ajudaDeCusto = ajudaDeCusto;
    }

    @Override
    public double calcularSalario() {
        return super.calcularSalario() + getAjudaDeCusto();
    }

    @Override
    public String toString() {
        return super.toString() + "\tAjuda de custo: " + getAjudaDeCusto();
    }
}

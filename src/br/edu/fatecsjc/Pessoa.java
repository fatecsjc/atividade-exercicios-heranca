package br.edu.fatecsjc;

public class Pessoa {

    private String nome;
    private String endereco;
    private String telefone;

    Pessoa() {
    }

    Pessoa(String nome, String endereco, String telefone) {
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
    }

    private String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    private String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    private String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Nome: " + getNome() + "\tEnderešo: " + getEndereco() + "\tTelefone: " + getTelefone();
    }
}

package br.edu.fatecsjc;

public class Empregado extends Pessoa {

    private int codigoSetor;
    private double salarioBase;
    private double impostos;

    Empregado() {
        super();
    }

    Empregado(String nome, String endereco, String telefone) {
        super(nome, endereco, telefone);
    }

    Empregado(String nome, String endereco, String telefone, int codigoSetor, double salarioBase, double impostos) {
        super(nome, endereco, telefone);
        this.codigoSetor = codigoSetor;
        this.salarioBase = salarioBase;
        this.impostos = impostos;
    }

    private int getCodigoSetor() {
        return codigoSetor;
    }

    void setCodigoSetor(int codigoSetor) {
        this.codigoSetor = codigoSetor;
    }

    private double getSalarioBase() {
        return salarioBase;
    }

    void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    private double getImpostos() {

        switch (getCodigoSetor()) {
            case 1:
                setImpostos(0.1);
                break;
            case 2:
                setImpostos(0.2);
                break;
            case 3:
                setImpostos(0.3);
                break;
            default:
                setImpostos(0);
        }

        return impostos;
    }

    void setImpostos(double impostos) {
        this.impostos = impostos;
    }

    public double calcularSalario(){

        return getSalarioBase() + getSalarioBase() * getImpostos();
    }

    @Override
    public String toString() {
        return super.toString() + "\tC�digo setor: " + getCodigoSetor() + "\tSal�rio base: " + getSalarioBase()
                + "\tImpostos: " + getImpostos() + "\tCalculo sal�rio: " + calcularSalario();
    }
}

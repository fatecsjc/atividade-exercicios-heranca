package br.edu.fatecsjc;

public class MainApplication {


    public static void main(String[] args) {

        Fornecedor fornecedor = new Fornecedor("Fornecedor1", "Endere�o1", "01234");

        Empregado empregado1 = new Empregado("Empregado1", "Endere�o1", "12345");
        Empregado empregado2 = new Empregado("Empregado2", "Endere�o2", "23456");
        Empregado empregado3 = new Empregado("Empregado3", "Endere�o3", "34567");

        fornecedor.setValorCredito(1500.0);
        fornecedor.setValorDivida(1000);

        Empregado[] empregados = new Empregado[3];

        empregados[0] = empregado1;
        empregados[1] = empregado2;
        empregados[2] = empregado3;

        System.out.println("\n" + fornecedor.toString() + "\n");

        empregado1.setCodigoSetor(1);
        empregado1.setSalarioBase(1500.0);
        empregado2.setCodigoSetor(2);
        empregado2.setSalarioBase(2000.0);
        empregado3.setCodigoSetor(1);
        empregado3.setSalarioBase(2500.0);

        for (Empregado empregado : empregados)
            System.out.println(empregado.toString() + "\tCalculo sal�rio: " + empregado.calcularSalario());

        Administrador administrador1 = new Administrador("Admin1", "Endere�o1", "45678");

        System.out.println("");
        administrador1.setCodigoSetor(2);
        administrador1.setSalarioBase(3000.00);
        administrador1.setImpostos(100.0);
        administrador1.setAjudaDeCusto(1000.00);
        administrador1.calcularSalario();
        System.out.println(administrador1.toString());

        Pessoa pessoa = new Pessoa("Robson", "Endere�o1", "56789");
        System.out.println(pessoa.toString());

    }

}

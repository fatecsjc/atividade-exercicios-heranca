package br.edu.fatecsjc;

public class Fornecedor extends Pessoa {

    private double valorCredito;
    private double valorDivida;

    public Fornecedor() {
    }

    Fornecedor(String nome, String endereco, String telefone) {
        super(nome, endereco, telefone);
    }

    public Fornecedor(String nome, String endereco, String telefone, double valorCredito, double valorDivida) {
        super(nome, endereco, telefone);
        this.valorCredito = valorCredito;
        this.valorDivida = valorDivida;
    }

    private double getValorCredito() {
        return valorCredito;
    }

    void setValorCredito(double valorCredito) {
        this.valorCredito = valorCredito;
    }

    private double getValorDivida() {
        return valorDivida;
    }

    void setValorDivida(double valorDivida) {
        this.valorDivida = valorDivida;
    }

    private double obterSaldor() {

        return getValorCredito() - getValorDivida();
    }

    @Override
    public String toString() {
        return super.toString() + "\tValor cr�dito: " + getValorCredito() + "\tValor d�vida: " + getValorDivida() + "\tSaldo: " + obterSaldor();
    }
}
